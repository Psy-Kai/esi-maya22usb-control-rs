use anyhow::{anyhow, Result};
use serde::{Deserialize, Serialize};

use crate::constant::levelbound;

pub type Level = u32;

const MIN: Level = 0;
const MAX: Level = 1000;

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Volume {
    level: Level,
}

impl Volume {
    pub fn new(level: Level) -> Result<Self> {
        if (MIN..=MAX).contains(&level) {
            Ok(Self { level })
        } else {
            Err(anyhow!("level out of bounds: {level}"))
        }
    }

    pub fn set_level(&mut self, level: Level) -> Result<&mut Self> {
        let new_self = Volume::new(level)?;
        self.level = new_self.level;
        Ok(self)
    }

    pub fn level(&self) -> Level {
        self.level
    }
}

impl Default for Volume {
    fn default() -> Self {
        Self { level: MAX }
    }
}

pub trait CalculatedVolume {
    fn as_input_volume_data(&self) -> u8;
    fn as_output_volume_data(&self) -> u8;
}

impl CalculatedVolume for Level {
    fn as_input_volume_data(&self) -> u8 {
        calc(*self, levelbound::r#in::MIN, levelbound::r#in::MAX)
    }
    fn as_output_volume_data(&self) -> u8 {
        calc(*self, levelbound::out::MIN, levelbound::out::MAX)
    }
}

pub fn calc(level: Level, min: u8, max: u8) -> u8 {
    let max_out = max - min;
    let volume_in_percentage = level as f32 / MAX as f32;
    let volume = (volume_in_percentage * max_out as f32) as u8;
    min + volume
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn new_with_too_high_value() {
        assert!(Volume::new(1001).is_err());
    }

    #[test]
    fn new_with_valid_value() {
        let value = 5;
        assert_eq!(Volume::new(value).unwrap().level, value);
    }
}
