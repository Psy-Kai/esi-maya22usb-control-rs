use serde::{Deserialize, Serialize};

use self::volume::Volume;

mod volume;

pub use volume::CalculatedVolume;
pub use volume::Level;

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct Channel {
    pub left: Volume,
    pub right: Volume,
    pub mute: bool,
}
