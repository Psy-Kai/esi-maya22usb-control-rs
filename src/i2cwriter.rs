use anyhow::ensure;
use hidapi::HidDevice;

const STRUCTURE_SIZE: usize = 32;

pub trait Write {
    fn write(&mut self, command: u8, data: u8) -> anyhow::Result<()>;
}

pub struct I2cWriter {
    hid_device: Option<HidDevice>,
}

impl I2cWriter {
    pub fn new(hid_device: HidDevice) -> Self {
        Self {
            hid_device: Some(hid_device),
        }
    }
    #[allow(dead_code)]
    pub fn dummy() -> Self {
        Self { hid_device: None }
    }
}

impl Write for I2cWriter {
    fn write(&mut self, command: u8, data: u8) -> anyhow::Result<()> {
        let mut buffer = [0u8; STRUCTURE_SIZE];

        buffer[1] = 0x12;
        buffer[2] = 0x34;
        buffer[3] = command;
        buffer[5] = 1;
        buffer[6] = data;
        buffer[22] = 0x80;

        match &mut self.hid_device {
            Some(hid_device) => write(hid_device, &buffer)?,
            None => simulate(&buffer),
        }
        Ok(())
    }
}

fn write(hid_device: &mut HidDevice, buffer: &[u8]) -> anyhow::Result<()> {
    let count = hid_device.write(buffer)?;
    ensure!(count == STRUCTURE_SIZE, "written only {count} bytes");
    Ok(())
}

fn simulate(buffer: &[u8]) {
    // log::trace!("write {command:02x} with {data:02x}");
    log::trace!("write {buffer:?}");
}
