use serde::{Deserialize, Serialize};

use channel::Channel;

pub mod channel;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum Input {
    LineIn = 4,
    Mic = 1,
    HiZ = 2,
    MicHiZ = 8,
}

impl Default for Input {
    fn default() -> Self {
        Self::Mic
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum Monitoring {
    Disabled = 1,
    Enabled = 5,
}

impl Default for Monitoring {
    fn default() -> Self {
        Self::Disabled
    }
}

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct Channels {
    pub input: Channel,
    pub output: Channel,
}

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct Model {
    pub input: Input,
    pub monitoring: Monitoring,
    pub channel: Channels,
}
