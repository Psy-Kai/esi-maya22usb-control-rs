use crate::{
    constant::command,
    i2cwriter::Write,
    model::{
        self,
        channel::{CalculatedVolume, Channel, Level},
    },
};

pub trait View {
    fn channel(&self) -> &Channel;
    fn left(&self) -> Level {
        self.channel().left.level()
    }
    fn right(&self) -> Level {
        self.channel().right.level()
    }
    fn mute(&self) -> bool {
        self.channel().mute
    }
}

pub trait InputView: View {
    fn input_type(&self) -> model::Input;
}

pub trait Changer: View {
    fn set_left(&mut self, level: Level) -> anyhow::Result<()>;
    fn set_right(&mut self, level: Level) -> anyhow::Result<()>;
    fn set_mute(&mut self, mute: bool) -> anyhow::Result<()>;
}

pub trait InputChanger: InputView + Changer {
    fn set_input_type(&mut self, input_type: model::Input) -> anyhow::Result<()>;
}

pub struct Input<'a> {
    input_type: &'a mut model::Input,
    channel: &'a mut Channel,
    writer: &'a mut dyn Write,
}

impl<'a> Input<'a> {
    pub fn new(
        input_type: &'a mut model::Input,
        channel: &'a mut Channel,
        writer: &'a mut dyn Write,
    ) -> Self {
        Self {
            input_type,
            channel,
            writer,
        }
    }
}

impl<'a> View for Input<'a> {
    fn channel(&self) -> &Channel {
        self.channel
    }
}

impl<'a> InputView for Input<'a> {
    fn input_type(&self) -> model::Input {
        self.input_type.clone()
    }
}

impl<'a> Changer for Input<'a> {
    fn set_left(&mut self, level: Level) -> anyhow::Result<()> {
        if !self.channel.mute {
            self.writer
                .write(command::VOLUME_IN_LEFT, level.as_input_volume_data())?;
        }
        self.channel.left.set_level(level)?;
        Ok(())
    }
    fn set_right(&mut self, level: Level) -> anyhow::Result<()> {
        if !self.channel.mute {
            self.writer
                .write(command::VOLUME_IN_RIGHT, level.as_input_volume_data())?;
        }
        self.channel.right.set_level(level)?;
        Ok(())
    }
    fn set_mute(&mut self, mute: bool) -> anyhow::Result<()> {
        let input_type = *self.input_type;
        let data = if mute {
            input_type as u8 | command::MUTE_MASK
        } else {
            input_type as u8
        };

        self.writer.write(command::SELECT_INPUT, data)?;
        self.channel.mute = mute;
        Ok(())
    }
}

impl<'a> InputChanger for Input<'a> {
    fn set_input_type(&mut self, input_type: model::Input) -> anyhow::Result<()> {
        self.writer.write(command::SELECT_INPUT, input_type as u8)?;
        *self.input_type = input_type;
        Ok(())
    }
}

pub struct Output<'a> {
    channel: &'a mut Channel,
    writer: &'a mut dyn Write,
}

impl<'a> Output<'a> {
    pub fn new(channel: &'a mut Channel, writer: &'a mut dyn Write) -> Self {
        Self { channel, writer }
    }
}

impl<'a> View for Output<'a> {
    fn channel(&self) -> &Channel {
        self.channel
    }
}

impl<'a> Changer for Output<'a> {
    fn set_left(&mut self, level: Level) -> anyhow::Result<()> {
        if !self.channel.mute {
            self.writer
                .write(command::VOLUME_OUT_LEFT, level.as_output_volume_data())?;
        }
        self.channel.left.set_level(level)?;
        Ok(())
    }
    fn set_right(&mut self, level: Level) -> anyhow::Result<()> {
        if !self.channel.mute {
            self.writer
                .write(command::VOLUME_OUT_RIGHT, level.as_output_volume_data())?;
        }
        self.channel.right.set_level(level)?;
        Ok(())
    }
    fn set_mute(&mut self, mute: bool) -> anyhow::Result<()> {
        if mute {
            self.writer
                .write(command::VOLUME_OUT_LEFT, 0.as_output_volume_data())?;
            self.writer
                .write(command::VOLUME_OUT_RIGHT, 0.as_output_volume_data())?;
        } else {
            self.writer.write(
                command::VOLUME_OUT_LEFT,
                self.channel.left.level().as_output_volume_data(),
            )?;
            self.writer.write(
                command::VOLUME_OUT_RIGHT,
                self.channel.right.level().as_output_volume_data(),
            )?;
        }
        self.channel.mute = mute;
        Ok(())
    }
}
