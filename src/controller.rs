use crate::{
    constant::command,
    i2cwriter::{I2cWriter, Write},
    model::{Model, Monitoring},
};

use self::channel::{Input, Output};

pub mod channel;

pub struct Controller {
    model: Model,
    writer: I2cWriter,
}

impl Controller {
    pub fn new(model: Model, mut writer: I2cWriter) -> Self {
        writer.write(command::UNMUTE_HEADPHONE, 0).unwrap();
        Self { model, writer }
    }
    pub fn monitoring(&self) -> Monitoring {
        self.model.monitoring
    }
    pub fn enable_monitoring(&mut self, value: Monitoring) -> anyhow::Result<()> {
        self.writer.write(command::MONITORING, value as u8)?;
        self.model.monitoring = value;
        Ok(())
    }
    pub fn input(&mut self) -> impl channel::InputChanger + '_ {
        Input::new(
            &mut self.model.input,
            &mut self.model.channel.input,
            &mut self.writer,
        )
    }
    pub fn output(&mut self) -> impl channel::Changer + '_ {
        Output::new(&mut self.model.channel.output, &mut self.writer)
    }
}

impl From<Controller> for Model {
    fn from(value: Controller) -> Self {
        value.model
    }
}
