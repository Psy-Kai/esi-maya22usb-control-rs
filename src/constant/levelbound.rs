pub mod out {
    pub const MIN: u8 = 110;
    pub const MAX: u8 = 110 + 145;
}

pub mod r#in {
    pub const MIN: u8 = 104;
    pub const MAX: u8 = 104 + 127;
}
