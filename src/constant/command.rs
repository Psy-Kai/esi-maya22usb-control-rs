pub const UNMUTE_HEADPHONE: u8 = 0x0d << 1;
pub const VOLUME_OUT_LEFT: u8 = (0x03 << 1) + 1;
pub const VOLUME_OUT_RIGHT: u8 = (0x04 << 1) + 1;
pub const VOLUME_IN_LEFT: u8 = 0x0e << 1;
pub const VOLUME_IN_RIGHT: u8 = 0x0f << 1;
pub const SELECT_INPUT: u8 = 0x15 << 1;
pub const MUTE_MASK: u8 = 0xc0;
pub const MONITORING: u8 = 0x16 << 1;
