use std::{
    fs::{create_dir_all, File},
    path::{Path, PathBuf},
};

use anyhow::anyhow;
use directories::ProjectDirs;

use crate::model::Model;

fn project_dirs() -> anyhow::Result<ProjectDirs> {
    ProjectDirs::from("com", "psy-kai", "ESI-Maya22USB-control").ok_or(anyhow!("no home directory"))
}

fn file_path(path: &Path) -> PathBuf {
    const FILE_NAME: &str = "config.json";
    path.join(FILE_NAME)
}

pub fn save(model: &Model) -> anyhow::Result<()> {
    let config_dir = project_dirs()?.config_dir().to_owned();
    create_dir_all(&config_dir)?;

    let file = File::create(file_path(&config_dir))?;
    serde_json::to_writer(file, &model)?;
    Ok(())
}

pub fn load() -> anyhow::Result<Model> {
    let file = File::open(file_path(project_dirs()?.config_dir()))?;
    let model: Model = serde_json::from_reader(file)?;
    Ok(model)
}
