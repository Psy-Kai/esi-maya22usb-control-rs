use std::{cell::RefCell, rc::Rc};

use constant::id::{PRODUCT, VENDOR};
use controller::{
    channel::{Changer, InputChanger, InputView, View},
    Controller,
};
use hidapi::HidApi;
use model::{channel::Level, Monitoring};
use slint::ComponentHandle;

use crate::model::Model;

mod constant;
mod controller;
mod i2cwriter;
mod model;
mod settings;
mod ui;

fn main() -> anyhow::Result<()> {
    std::env::set_var("RUST_LOG", std::env!("CARGO_PKG_NAME").replace('-', "_"));
    pretty_env_logger::init();

    let model: Model = settings::load().unwrap_or_default();

    #[cfg(not(feature = "dummy_i2cwriter"))]
    let Ok(controller) = HidApi::new()?
        .open(VENDOR, PRODUCT)
        .map_err(|error| {
            log::error!(
                "Failed to open hid device with {VENDOR:04x}:{PRODUCT:04x} (vendor-id:product-id)"
            );
            error
        })
        .map(i2cwriter::I2cWriter::new)
        .map(|i2c_writer| Controller::new(model, i2c_writer))
        .map(|controller| Rc::new(RefCell::new(controller)))
    else {
        let message_box_error = ui::MessageBox::new()?;

        message_box_error.on_close(|| {
            let _ = slint::quit_event_loop();
        });

        message_box_error.run()?;
        return Ok(());
    };
    #[cfg(feature = "dummy_i2cwriter")]
    let controller = Rc::new(RefCell::new(Controller::new(
        model,
        i2cwriter::I2cWriter::dummy(),
    )));

    let app = ui::App::new()?;
    let side_pane_adapter = app.global::<ui::SidePaneAdapter>();
    let channels_adapter = app.global::<ui::ChannelsAdapter>();
    {
        let mut controller = controller.borrow_mut();
        side_pane_adapter.set_selected_input(controller.input().input_type().into());
        side_pane_adapter.set_monitoring(match controller.monitoring() {
            Monitoring::Disabled => false,
            Monitoring::Enabled => true,
        });

        {
            let input = controller.input();
            channels_adapter.set_input(ui::ChannelData {
                left: -(input.left() as f32),
                mute: input.mute(),
                right: -(input.right() as f32),
            });
        }
        {
            let output = controller.output();
            channels_adapter.set_output(ui::ChannelData {
                left: -(output.left() as f32),
                mute: output.mute(),
                right: -(output.right() as f32),
            });
        }
    }

    adapter_connect_controller(
        &app,
        &controller,
        |side_pane_adapter: ui::SidePaneAdapter, controller| {
            side_pane_adapter.on_input_selected(move |input| {
                controller.with_mut_ref(move |controller| {
                    let _ = controller.input().set_input_type(input.into());
                })
            })
        },
    );
    adapter_connect_controller(
        &app,
        &controller,
        |side_pane_adapter: ui::SidePaneAdapter, controller| {
            side_pane_adapter.on_monitoring_toggled(move |monitoring| {
                controller.with_mut_ref(move |controller| {
                    let _ = controller.enable_monitoring(if monitoring {
                        Monitoring::Enabled
                    } else {
                        Monitoring::Disabled
                    });
                })
            })
        },
    );

    adapter_connect_controller(
        &app,
        &controller,
        |channels_adapter: ui::ChannelsAdapter, controller| {
            channels_adapter.on_input_left(move |left| {
                controller.with_mut_ref(move |controller| {
                    let _ = controller.input().set_left((-left) as Level);
                })
            })
        },
    );
    adapter_connect_controller(
        &app,
        &controller,
        |channels_adapter: ui::ChannelsAdapter, controller| {
            channels_adapter.on_input_right(move |right| {
                controller.with_mut_ref(move |controller| {
                    let _ = controller.input().set_right((-right) as Level);
                })
            })
        },
    );
    adapter_connect_controller(
        &app,
        &controller,
        |channels_adapter: ui::ChannelsAdapter, controller| {
            channels_adapter.on_input_mute(move |mute| {
                controller.with_mut_ref(move |controller| {
                    let _ = controller.input().set_mute(mute);
                })
            })
        },
    );

    adapter_connect_controller(
        &app,
        &controller,
        |channels_adapter: ui::ChannelsAdapter, controller| {
            channels_adapter.on_output_left(move |left| {
                controller.with_mut_ref(move |controller| {
                    let _ = controller.output().set_left((-left) as Level);
                })
            })
        },
    );
    adapter_connect_controller(
        &app,
        &controller,
        |channels_adapter: ui::ChannelsAdapter, controller| {
            channels_adapter.on_output_right(move |right| {
                controller.with_mut_ref(move |controller| {
                    let _ = controller.output().set_right((-right) as Level);
                })
            })
        },
    );
    adapter_connect_controller(
        &app,
        &controller,
        |channels_adapter: ui::ChannelsAdapter, controller| {
            channels_adapter.on_output_mute(move |mute| {
                controller.with_mut_ref(move |controller| {
                    let _ = controller.output().set_mute(mute);
                })
            })
        },
    );

    app.show()?;

    slint::run_event_loop()?;

    let model: Model = Rc::into_inner(controller)
        .expect("App closed and no callback running")
        .into_inner()
        .into();
    settings::save(&model)?;

    Ok(())
}

impl From<ui::InputType> for model::Input {
    fn from(value: ui::InputType) -> Self {
        match value {
            ui::InputType::LineIn => model::Input::LineIn,
            ui::InputType::Mic => model::Input::Mic,
            ui::InputType::HiZ => model::Input::HiZ,
            ui::InputType::MicHiZ => model::Input::MicHiZ,
        }
    }
}

impl From<model::Input> for ui::InputType {
    fn from(value: model::Input) -> Self {
        match value {
            model::Input::LineIn => ui::InputType::LineIn,
            model::Input::Mic => ui::InputType::Mic,
            model::Input::HiZ => ui::InputType::HiZ,
            model::Input::MicHiZ => ui::InputType::MicHiZ,
        }
    }
}

type StrongController = Rc<RefCell<Controller>>;

struct WeakController {
    inner: std::rc::Weak<RefCell<Controller>>,
}

impl WeakController {
    fn new(strong: &StrongController) -> Self {
        Self {
            inner: Rc::downgrade(strong),
        }
    }

    fn with_mut_ref(&self, f: impl Fn(&mut Controller)) {
        let strong = self.inner.upgrade().unwrap();
        let mut mut_ref = RefCell::borrow_mut(&strong);
        f(&mut mut_ref);
    }
}

fn adapter_connect_controller<'a, TView, TAdapter>(
    view: &'a TView,
    controller: &StrongController,
    f: impl Fn(TAdapter, WeakController),
) where
    TView: ComponentHandle,
    TAdapter: slint::Global<'a, TView>,
{
    let adapter = view.global::<TAdapter>();
    let weak_controller = WeakController::new(controller);
    f(adapter, weak_controller);
}
