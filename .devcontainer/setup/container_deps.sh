#/bin/sh

DEPS="
    pkgconf
    libusb-1.0-0-dev
    libudev-dev
"

sudo apt update && sudo apt -y upgrade && sudo apt -y install $DEPS
